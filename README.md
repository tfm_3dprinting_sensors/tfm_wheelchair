# tfm_wheelchair

:exclamation: Please, to use this repository start with doing the setup defined in https://gitlab.com/tfm_3dprinting_sensors/tfm_workspace_setup.


## Repository structure

This repository contains three ROS packages:

- **wheelchair_description**: it contains the wheelchair model that has been designed with Solidworks and exported through [Solidworks to URDF Exporter addon](http://wiki.ros.org/sw_urdf_exporter/Tutorials/Export%20an%20Assembly). This addon already generates the package.
- **wheelchair_control**: it uses the ROS package [differential drive controller](http://wiki.ros.org/diff_drive_controller) with a .yaml config file with the paramaters of the controller for the specific applicaiton.
- **wheelchair_bringup**: package with the launch file to start the gazebo world with the wheelchair model and the control listening to the inputs from Arduino UNO board topics being broadcasted to move the wheelchair.

## Authors and acknowledgment
Oriol Vendrell

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.
